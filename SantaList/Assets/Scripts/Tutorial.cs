﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {

    private Animator tutorialAnim;
    private Text tutorialText;
    private Director buttons;
    private int frame;
    private string auxText;
    public AudioSource myAudio;

	// Use this for initialization
	void Start () {
        tutorialAnim = GameObject.Find("Tutorial").GetComponent<Animator>();
        tutorialText = GameObject.Find("TutorialText").GetComponent<Text>();
        buttons = GameObject.Find("Director").GetComponent<Director>();
        frame = 1;
        auxText = "";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NextFrame() {
        myAudio.Play();
        frame++;
        tutorialAnim.SetInteger("Frame", frame);
        switch (frame) {
            case 2:
                auxText = "These are the guidelines you have to follow to know how nice children should be.";
                break;
            case 3:
                auxText = "Here you can see how each child has been during the year.";
                break;
            case 4:
                auxText = "Rate if they have been good or bad...";
                break;
            case 5:
                auxText = "...to put them on the naughty list.";
                break;
            case 6:
                auxText = "Finally, here you can see how many children you have left to finish.";
                break;
            case 7:
                auxText = "";
                buttons.StartGame();
                break;
        }
        tutorialText.text = auxText;
    }

}
