﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputHandler : MonoBehaviour {

    [SerializeField] private Camera mainCamera;
    private RaycastHit tapHit;

    // Use this for initialization
    void Start() {
        if (!mainCamera) mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update() {
        tapHit = new RaycastHit();
        try {
            if (Input.GetMouseButtonDown(0)) {
                Ray tapRay = mainCamera.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(mainCamera.transform.position, tapRay.direction * 200, new Color(255, 0, 0, 1), 3);

                // All colliders have to be 3D
                if (Physics.Raycast(tapRay, out tapHit, 200)) {
                    switch (tapHit.transform.tag) {
                        case "Enemy":
                            break;
                        case "Fish":
                            break;
                        case "Coral":
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        catch (System.Exception e) {
            throw e;
        }
        finally {
            if (EventSystem.current) {
                EventSystem e = EventSystem.current;
                if (e.IsPointerOverGameObject() && Input.GetMouseButtonDown(0)) { }
                else if (tapHit.transform) { }
                else if (Input.GetMouseButtonDown(0)) { }

            }
        }
    }
}
