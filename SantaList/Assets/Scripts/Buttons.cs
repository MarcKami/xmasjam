﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {

    public void Load(string name) {
        SceneManager.LoadScene(name);
    }
        
    public void Exit() {
        Application.Quit();
        Debug.Log("Quit");
    }
}
