﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaBehaviours {

    public int grade;
    public bool homework;
    public bool swearwords;
    public bool obey;
    public bool vegetables;

    public void MakeGuidelines() {
        grade = 5;
        homework = true;
        swearwords = false;
        obey = true;
        vegetables = true;
    }

    public void MakeBehavior() {
        //Grade
        grade = Random.Range(1, 10);
        //HomeWork
        int aux = Random.Range(0, 6);
        if (aux > 0)
            homework = true;
        else
            homework = false;
        //Swearwords
        aux = Random.Range(0, 6);
        if (aux > 0)
            swearwords = false;
        else
            swearwords = true;
        //Obey
        aux = Random.Range(0, 6);
        if (aux > 0)
            obey = true;
        else
            obey = false;
        //Vegetables
        aux = Random.Range(0, 6);
        if (aux > 0)
            vegetables = true;
        else
            vegetables = false;
    }

}
