﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Director : MonoBehaviour {

    public enum State {Tutorial, InGame, EndGame}

    public State state;
    private bool win;
    public SantaBehaviours guidelines;
    //Kids
    public Kid[] kid;
    public int activeKid;
    public int errors;
    //UI
    public GameObject tutorialUI;
    public GameObject inGameUI;
    public GameObject endGameUI;
    private float timer;
    private Text remainsText;
    private Text winImage;
    private Text loseImage;
    private Text timerText;
    private Text mistakesText;
    private int offsetNaughty;
    private int offsetNice;
    private int offsetNaughty2;
    private int offsetNice2;
    public AudioSource naughtyAudio;
    public AudioSource niceAudio;

    // Use this for initialization
    void Start () {
        //Init State
        state = State.Tutorial;
        timer = 0.0f;
        win = false;

        //Guidelines
        guidelines = new SantaBehaviours();
        guidelines.MakeGuidelines();

        //Kids
        GameObject[] list;
        list = GameObject.FindGameObjectsWithTag("Kid");
        kid = new Kid[list.Length];
        for (int i = 0; i < list.Length; i++) kid[i] = list[i].GetComponent<Kid>();

        //UI
        tutorialUI = GameObject.Find("TutorialUI");
        inGameUI = GameObject.Find("InGameUI");
        endGameUI = GameObject.Find("EndGameUI");
        timerText = GameObject.Find("Timer").GetComponent<Text>();
        remainsText = GameObject.Find("Remaining").GetComponent<Text>();
        mistakesText = GameObject.Find("Mistakes").GetComponent<Text>();
        winImage = GameObject.Find("Win").GetComponent<Text>();
        loseImage = GameObject.Find("Lose").GetComponent<Text>();
        winImage.gameObject.SetActive(false);
        loseImage.gameObject.SetActive(false);

        //ActiveKid
        activeKid = 0;

        //Errors
        errors = 0;

        //Offset
        offsetNaughty = 0;
        offsetNice = 0;
        offsetNaughty2 = 0;
        offsetNice2 = 0;
    }

    // Update is called once per frame
    void Update() {
        switch (state) {
            case State.Tutorial:
                inGameUI.SetActive(false);
                endGameUI.SetActive(false);

                break;
            case State.InGame:
                timer += Time.deltaTime;
                endGameUI.SetActive(false);
                for (int i = 0; i < kid.Length; i++) {
                    if (activeKid == i) {
                        kid[i].gameObject.SetActive(true);
                        kid[i].kidText.gameObject.SetActive(true);
                    }
                    else {
                        kid[i].gameObject.SetActive(false);
                        kid[i].kidText.gameObject.SetActive(false);
                    }
                }
                break;
            case State.EndGame:
                SetTimeMistakes();
                kid[activeKid].gameObject.SetActive(false);
                kid[activeKid].kidText.gameObject.SetActive(false);
                if (win)  winImage.gameObject.SetActive(true);
                else loseImage.gameObject.SetActive(true);

                break;
        }
    }

    public void Stamp(bool s) {
        kid[activeKid].isNaughty = s;

        CompareBehaviors(s);

        if (s) {
            naughtyAudio.Play();
            kid[activeKid].PutList(offsetNaughty);
            kid[activeKid].PutSanta(offsetNaughty2);
            offsetNaughty += 13;
            offsetNaughty2 += 8;
        }
        else {
            niceAudio.Play();
            kid[activeKid].PutList(offsetNice);
            kid[activeKid].PutSanta(offsetNice2);
            offsetNice += 13;
            offsetNice2 += 8;
        }
        
        if (activeKid <= 18) {
            activeKid++;
            remainsText.text = "Remaining Kids: " + (kid.Length - activeKid);
        }
        else {
            state = State.EndGame;
            if (errors < 3) win = true;
            inGameUI.SetActive(false);
            endGameUI.SetActive(true);
        }

    }

    void CompareBehaviors(bool s) {
        bool naughty = false;

        if (kid[activeKid].behaviours.grade < guidelines.grade) naughty = true;
        else if (kid[activeKid].behaviours.homework != guidelines.homework) naughty = true;
        else if (kid[activeKid].behaviours.swearwords != guidelines.swearwords) naughty = true;
        else if (kid[activeKid].behaviours.obey != guidelines.obey) naughty = true;
        else if (kid[activeKid].behaviours.vegetables != guidelines.vegetables) naughty = true;

        if (naughty != s) {
            errors++;
            kid[activeKid].santaCorrect = false;
        }
    }

    void SetTimeMistakes() {
        float minutes = Mathf.Floor(timer / 60);
        float seconds = Mathf.RoundToInt(timer % 60);

        string min = minutes.ToString();
        string sec = Mathf.RoundToInt(seconds).ToString();

        if (minutes < 10) min = "0" + minutes.ToString();
        if (seconds < 10) sec = "0" + Mathf.RoundToInt(seconds).ToString();

        timerText.text = min + " : " + sec;

        mistakesText.text = "MISTAKES: " + errors;
    }

    public void StartGame() {
        tutorialUI.SetActive(false);
        inGameUI.SetActive(true);
        state = State.InGame;
    }
}
