﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kid : MonoBehaviour {

    public new string name;
    public int age;
    public SantaBehaviours behaviours;
    public Text kidText;
    public Text santaList;
    public Text kidList;
    public bool isNaughty;
    public bool santaCorrect;

    // Use this for initialization
    void Start () {
        isNaughty = false;
        santaCorrect = true;

        behaviours = new SantaBehaviours();
        behaviours.MakeBehavior();
        age = Random.Range(5, 11);

        //Text
        string aux = gameObject.name + "Text";
        kidText = GameObject.Find(aux).GetComponent<Text>();
        PutText();

        aux = gameObject.name + "List";
        kidList = GameObject.Find(aux).GetComponent<Text>();

        aux = gameObject.name + "Santa";
        santaList = GameObject.Find(aux).GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void PutText() {
        //General
        kidText.text = "NAME: " + name + "\n" +
                       "AGE: " + age + "\n\n" +
                       "SCHOOL GRADE: " + behaviours.grade + "\n";
        //Homework
        if (behaviours.homework)
            kidText.text += "DO HOMEWORK: Yes\n";
        else
            kidText.text += "DO HOMEWORK: No\n";
        //Swearwords
        if (behaviours.swearwords)
            kidText.text += "SAY SWEARWORDS: Yes\n";
        else
            kidText.text += "SAY SWEARWORDS: No\n";
        //Obey
        if (behaviours.obey)
            kidText.text += "OBEY PARENTS: Yes\n";
        else
            kidText.text += "OBEY PARENTS: No\n";
        //Vegetables
        if (behaviours.vegetables)
            kidText.text += "EAT VEGETABLES: Yes\n";
        else
            kidText.text += "EAT VEGETABLES: No\n";
    }

    public void PutList(int offset) {
        kidList.text = name;
        if (!santaCorrect) {
            if (isNaughty) kidList.color = new Color(0.0f, 0.65f, 0.00f); //GREEN COLOR
            else kidList.color = new Color(0.7f, 0.01f, 0.01f); //RED COLOR
        }
        if (isNaughty) kidList.rectTransform.localPosition = new Vector3(kidList.rectTransform.position.x - 85.0f, kidList.rectTransform.position.y - offset);
        else kidList.rectTransform.localPosition = new Vector3(kidList.rectTransform.position.x + 85.0f, kidList.rectTransform.position.y - offset);
    }

    public void PutSanta(int offset) {
        Debug.Log("PutSanta");
        santaList.text = name;
        if (isNaughty) santaList.rectTransform.localPosition = new Vector3(santaList.rectTransform.position.x - 45.0f, santaList.rectTransform.position.y - offset);
        else santaList.rectTransform.localPosition = new Vector3(santaList.rectTransform.position.x + 45.0f, santaList.rectTransform.position.y - offset);
    }

}
